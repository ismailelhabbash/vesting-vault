//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract BatchTransfer {
    using SafeERC20 for IERC20;

    function dispatch(
        IERC20 token,
        address[] memory receivers,
        uint256[] memory amounts
    ) external {
        for (uint256 i; i < receivers.length; i++) {
           token.safeTransferFrom(msg.sender, receivers[i], amounts[i]);
        }
    }
}
