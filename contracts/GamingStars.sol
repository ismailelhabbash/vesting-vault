//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import '@openzeppelin/contracts/access/Ownable.sol';

contract GamingStars is ERC20('Gaming Stars', 'GAMES'), Ownable {
    constructor() {
        _mint(msg.sender, 10_000_000);
    }
}
